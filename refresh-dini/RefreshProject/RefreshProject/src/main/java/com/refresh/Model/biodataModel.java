package com.refresh.Model;

import java.sql.Date;

import javax.annotation.Generated;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="x_biodata")
public class biodataModel {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private Long id;
	
	@Column(name="name", length = 100, nullable = false)
	private String name;
	
	@Column(name="gender", nullable = false)
	private Boolean gender;
	
	@Column(name="birth_date", nullable = false)
	private Date birthDate;
	
	@Column(name="number", nullable = false)
	private Integer number;
	
	@Column(name="address", nullable = false)
	private String address;
	
	@Column(name="demam", nullable = false)
	private Boolean demam;
	
	@Column(name="batuk", nullable = false)
	private Boolean batuk;
	
	@Column(name="sesak_nafas", nullable = false)
	private Boolean sesakNafas;
	
	@Column(name="riwayat_perjalanan", nullable = false)
	private Boolean riwayatPerjalanan;
	
	@Column(name="kota_terjangkit", nullable = false)
	private String kotaTerjangkit;
	
	@Column(name="riwayat_kontak_covid19", nullable = false)
	private Boolean riwayatKontakCovid19;
	
	@Column(name="mengunjungi_fasilitas_kesehatan", nullable = false)
	private Boolean mengunjungiFasilitasKesehatan;
	
	@Column(name="riwayat_kontak_denganhewan", nullable = false)
	private Boolean riwayatKontakdenganHewan;
	
	@Column(name="riwayat_demam_38", nullable = false)
	private Boolean riwayatDemam38;
	
	@Column(name = "is_delete",columnDefinition = "boolean default false",  nullable = false)
    private Boolean isDelete;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getGender() {
		return gender;
	}

	public void setGender(Boolean gender) {
		this.gender = gender;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Boolean getDemam() {
		return demam;
	}

	public void setDemam(Boolean demam) {
		this.demam = demam;
	}

	public Boolean getBatuk() {
		return batuk;
	}

	public void setBatuk(Boolean batuk) {
		this.batuk = batuk;
	}

	public Boolean getSesakNafas() {
		return sesakNafas;
	}

	public void setSesakNafas(Boolean sesakNafas) {
		this.sesakNafas = sesakNafas;
	}

	public Boolean getRiwayatPerjalanan() {
		return riwayatPerjalanan;
	}

	public void setRiwayatPerjalanan(Boolean riwayatPerjalanan) {
		this.riwayatPerjalanan = riwayatPerjalanan;
	}

	public String getKotaTerjangkit() {
		return kotaTerjangkit;
	}

	public void setKotaTerjangkit(String kotaTerjangkit) {
		this.kotaTerjangkit = kotaTerjangkit;
	}

	public Boolean getRiwayatKontakCovid19() {
		return riwayatKontakCovid19;
	}

	public void setRiwayatKontakCovid19(Boolean riwayatKontakCovid19) {
		this.riwayatKontakCovid19 = riwayatKontakCovid19;
	}

	public Boolean getMengunjungiFasilitasKesehatan() {
		return mengunjungiFasilitasKesehatan;
	}

	public void setMengunjungiFasilitasKesehatan(Boolean mengunjungiFasilitasKesehatan) {
		this.mengunjungiFasilitasKesehatan = mengunjungiFasilitasKesehatan;
	}

	public Boolean getRiwayatKontakdenganHewan() {
		return riwayatKontakdenganHewan;
	}

	public void setRiwayatKontakdenganHewan(Boolean riwayatKontakdenganHewan) {
		this.riwayatKontakdenganHewan = riwayatKontakdenganHewan;
	}

	public Boolean getRiwayatDemam38() {
		return riwayatDemam38;
	}

	public void setRiwayatDemam38(Boolean riwayatDemam38) {
		this.riwayatDemam38 = riwayatDemam38;
	}

	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}
	
	
}
