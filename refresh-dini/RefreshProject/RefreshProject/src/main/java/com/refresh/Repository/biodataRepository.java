package com.refresh.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.refresh.Model.biodataModel;

public interface biodataRepository extends JpaRepository<biodataModel, Long>{

	@Query("select b from biodataModel b order by b.id")
	List<biodataModel>semuabiodata();
	
	@Query("select b from biodataModel b where b.id=?1")
	biodataModel biodatabyid (Long ids);
	
	@Modifying
	@Query("update biodataModel set isDelete=true where id=?1")
	int deleteflag(Long ids);
}
