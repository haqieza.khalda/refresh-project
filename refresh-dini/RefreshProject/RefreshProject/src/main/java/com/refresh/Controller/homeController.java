package com.refresh.Controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.websocket.server.PathParam;

import org.hibernate.annotations.OnDelete;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.refresh.Model.biodataModel;
import com.refresh.Service.biodataService;


@Controller
public class homeController {

	@Autowired
	private biodataService bs;
	
	@RequestMapping("/")
	public String home() {
		return "home";
	}
	
	@RequestMapping("listdata")
	public String datalist(Model model) {
		List<biodataModel>semua = bs.semuabiodata();
		model.addAttribute("semuabiodata", semua);
		return "listdata";
	}
	
	@RequestMapping("dataadd")
	public String dataadd(Model model) {
		List<biodataModel>semualist = bs.semuabiodata();
		model.addAttribute("semua list", semualist);
		return "dataadd";
	}
	
	@RequestMapping("dataedit/{ids}")
	public String dataedit(Model model, @PathVariable(name="ids")Long ids) {
		biodataModel biodatabyid = bs.biodatabyid(ids);
		model.addAttribute("biodatabyid", biodatabyid);
		return "dataedit";
	}
	
	@RequestMapping("biodatadelete/{ids}")
	public String datadelete(Model model, @PathVariable(name="ids")Long ids) {
		biodataModel biodatabyid = bs.biodatabyid(ids);
		model.addAttribute("biodatabyid", biodatabyid);
		return "datadelete";
	}
	
	@ResponseBody
	@RequestMapping("simpandata")
	public String simpandata(@ModelAttribute()biodataModel biodataModel) {
		biodataModel.setIsDelete(false);
		bs.simpanbiodata(biodataModel);
		return "";	
	}
	
	@ResponseBody
	@RequestMapping("ubahdata")
	public String ubahdata(@ModelAttribute()biodataModel biodataModel) {
		biodataModel.setIsDelete(false);
		bs.simpanbiodata(biodataModel);
		return "";
	}
	
	@ResponseBody
	@RequestMapping("hapusdata/{ids}")
	public String hapusdata(@PathVariable(name="ids")Long ids, @ModelAttribute biodataModel biodataModel) {
		biodataModel biodatabyid = bs.biodatabyid(ids);
		bs.simpanbiodata(biodatabyid);
		bs.hapusbiodata(ids);
		return "";
	}
	
	@InitBinder
	public void initBinder(WebDataBinder binder) {
	    CustomDateEditor editor = new CustomDateEditor(new SimpleDateFormat("yy-mm-dd"), true);
	    binder.registerCustomEditor(Date.class, editor);
	}
	
}
