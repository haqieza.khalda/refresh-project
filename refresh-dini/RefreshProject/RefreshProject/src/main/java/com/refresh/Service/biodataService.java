package com.refresh.Service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.refresh.Model.biodataModel;

@Service
@Transactional
public class biodataService {

	@Autowired
	private biodataService br;
	
	public List<biodataModel>semuabiodata(){
		return br.semuabiodata();
	}
	
	public biodataModel biodatabyid (Long ids) {
		return br.biodatabyid(ids);
	}
	
	public biodataModel simpanbiodata(biodataModel biodataModel) {
		return br.simpanbiodata(biodataModel);
	}
	
	public void hapusbiodata(Long ids) {
		br.hapusbiodata(ids);
		
	}
}
