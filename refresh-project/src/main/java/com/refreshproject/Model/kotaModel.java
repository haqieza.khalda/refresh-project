package com.refreshproject.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
	
@Entity
@Table (name="kota")
public class kotaModel{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column (name="id", length = 11, nullable = false)
	private int id;
	
	@Column (name="namaKota", length = 100, nullable = false)
	private String namaKota;
		
	@Column (name="is_delete", columnDefinition = "boolean default false", nullable = false)
	private boolean isDelete;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNamaKota() {
		return namaKota;
	}

	public void setNamaKota(String namaKota) {
		this.namaKota = namaKota;
	}

	public boolean isDelete() {
		return isDelete;
	}

	public void setDelete(boolean isDelete) {
		this.isDelete = isDelete;
	}

}
