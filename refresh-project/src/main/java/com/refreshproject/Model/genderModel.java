package com.refreshproject.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table (name="gender")
public class genderModel {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column (name="id", length = 11, nullable = false)
	private int id;
	
	
	@Column (name="nameGender", length = 100, nullable = false)
	private String nameGender;
	
	@Column (name="is_delete", columnDefinition = "boolean default false", nullable = false)
	private boolean isDelete;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNameGender() {
		return nameGender;
	}

	public void setNameGender(String nameGender) {
		this.nameGender = nameGender;
	}

	public boolean isDelete() {
		return isDelete;
	}

	public void setDelete(boolean isDelete) {
		this.isDelete = isDelete;
	}
	
}