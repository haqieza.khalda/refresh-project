package com.refreshproject.Controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.refreshproject.Model.dataModel;
import com.refreshproject.Service.dataService;

@Controller
public class dataController {
	
	@Autowired
	private dataService rs;
	
	@RequestMapping("datalist")
	public String datalist(Model model) {
		List<dataModel>semuadata=rs.semuadata();
		model.addAttribute("semuadata", semuadata);
		return "data/datalist";
	}
	
	@RequestMapping("data")
	public String data() {
		return "data/data";
	}
	
	@RequestMapping("dataadd")
	public String dataadd() {
		return "data/dataadd";
	}
	
	@ResponseBody
	@RequestMapping("simpandata")
	public String simpandata(@ModelAttribute()dataModel dataModel) {
		//cek nama
		String ceknama=rs.ceknama(dataModel.getNama());
		
		String hasil="0";
		if (ceknama==null) {
			rs.simpandata(dataModel);
		} else {
			hasil="1";
		}
		return hasil;
	}
		
	@ResponseBody
	@RequestMapping("ubahdata")
	public String ubahdata(@ModelAttribute()dataModel dataModel) {
		//set create
		dataModel databyid = rs.databyid(dataModel.getId());
		
		//cek nama
		String ceknama=null;
		if (dataModel.getNama().equals(databyid.getNama())) {
			//'do nothing'
		} else {
			ceknama=rs.ceknama(dataModel.getNama());
		}
		
		String hasil="0";
		if (ceknama==null) {
			rs.simpandata(dataModel);
		} else {
			hasil="1";
		}
		return hasil;
	}
	
	@ResponseBody
	@RequestMapping("hapusdata/{ids}")
	public String hapusdata(@ModelAttribute()dataModel dataModel,
			@PathVariable(name = "ids")int ids) {
		dataModel databyid = rs.databyid(ids);
		rs.simpandata(databyid);
		rs.hapusdata(ids);
		return "";
	}
	
	@RequestMapping("dataedit/{ids}")
	public String dataedit(Model model,
			@PathVariable(name="ids")int ids) {
		dataModel databyid=rs.databyid(ids);
		model.addAttribute("databyid", databyid);
		return "data/dataedit";
	}
	
	@RequestMapping("datadelete/{ids}")
	public String datadelete(Model model,
			@PathVariable(name="ids")int ids) {
		dataModel databyid=rs.databyid(ids);
		model.addAttribute("databyid", databyid);
		return "data/datadelete";
	}
	
	@InitBinder
	public void initBinder(WebDataBinder binder) {
	    CustomDateEditor editor = new CustomDateEditor(new SimpleDateFormat("yy-mm-dd"), true);
	    binder.registerCustomEditor(Date.class, editor);
	}

}
