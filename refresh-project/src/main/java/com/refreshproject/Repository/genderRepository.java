package com.refreshproject.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.refreshproject.Model.genderModel;

public interface genderRepository extends JpaRepository<genderModel, Integer> {
	@Query("select g from genderModel g where g.isDelete=false order by g.id")
	List<genderModel>semuagender();
	
	@Query("select g from genderModel g where g.id=?1")
	genderModel genderbyid(int ids);
}
