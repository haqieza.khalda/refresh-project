package com.refreshproject.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.refreshproject.Model.dataModel;

public interface dataRepository extends JpaRepository<dataModel, Integer>{
	
	@Query("select d from dataModel d where d.isDelete=false order by d.id") //si karywanModel disingkat menjadi "e"
	List<dataModel>semuadata();
	
	@Query("select d from dataModel d where d.id=?1")
	dataModel databyid(int ids);

	@Modifying
	@Query("update dataModel set isDelete = true where id=?1")
	int deleteflag(int ids);
	
	@Query("select d.nama from dataModel d where lower(d.nama)=lower(?1) and d.isDelete=false")
	String ceknama(String nama);
}
