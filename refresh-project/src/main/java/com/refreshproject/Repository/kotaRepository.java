package com.refreshproject.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.refreshproject.Model.kotaModel;

public interface kotaRepository extends JpaRepository<kotaModel, Integer>{
	@Query("select k from kotaModel k where k.isDelete=false order by k.id")
	List<kotaModel>semuakota();
	
	@Query("select k from kotaModel k where k.id=?1")
	kotaModel kotabyid(int ids);
}
