package com.refreshproject.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.refreshproject.Model.dataModel;
import com.refreshproject.Repository.dataRepository;

@Service
@Transactional
public class dataService {
	@Autowired
	private dataRepository dr;
	
	//by custom
	public List<dataModel> semuadata(){
		return dr.semuadata();
	}
	
	//save by jpa
	public dataModel simpandata(dataModel dataModel) {
		return dr.save(dataModel);
	}
	
	public dataModel databyid(int ids) {
		return dr.databyid(ids);
	}
	
	public void hapusdata(int ids) {
		dr.deleteflag(ids);
	}
	
	public String ceknama(String nama) {
		return dr.ceknama(nama);
	}
}
